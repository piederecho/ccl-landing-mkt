define([
], function () {

  function menu() {
    $('.BurgerMenu').on('click', function(){
      $('body').toggleClass('is-menuOpened');
      if($('body').hasClass('is-menuOpened')){
        $('.MenuWeb').fadeIn();
      } else {
        $('.MenuWeb').fadeOut();
      }
    });
  };

  return menu;
});


