require([
  'jquery',
  'menu',
  'owlslider',
  'alphanumeric'
  ], function ($, menu, owlslider, alphanumeric) {

    var $doc = $(document);

		function init() {
      if ($(window).width() < 1200 ){
        $('.videoBg').remove();
      }
      menu();
      sliderTeachers();
      showSections();
      showSectionsPC();
      sendInfo();
      popup();
      $('input[type="text"]').alpha();
      $('input[type="tel"]').numeric();
    }

    function sliderTeachers(){
    	$('.owl-carousel').owlCarousel({
			    loop:true,
			    margin:10,
			    nav:true,
			    responsive:{
			        0:{
			            items:1
			        },
              1200:{
                items: 2,
                margin: 25
              }
			    }
			})
      
    }

    function showSections(){
    	$('.info__container__title').on('click', function(){
    		$(this).parent().find('.info__container__content').slideToggle();
    	})
    }

    function showSectionsPC(){
    	$('.diplomado__info__nav li').on('click', function(){
    		$('.info__container .info__container__content').slideUp();
    		$('.diplomado__info__nav li').removeClass('diplomado__info__nav--active');
    		$(this).addClass('diplomado__info__nav--active');
    		var show = $(this).data('show');
    		var section = $(`.${show} .info__container__content`);
    		section.slideDown();
    	})
    }

    function sendInfo(){
      $('#formularioMKT').submit(function(event){

        event.preventDefault(); 
        $('.form__sendMsg').fadeIn();

        $.ajax({
          type: 'POST',
          url: $(this).attr('action'),
          data: $(this).serialize(),
          timeout: 10000,
          success: function(data){
            console.log(":D")
            window.location.href = "thanks.html";
          },
          error: function(){
            console.log(':(')
          }

        })
      });
    }
    function popup(){
      $('.popup__close').on('click', function(){
        $('.popup').fadeOut();
      })
      $('.form__tyc__button').on('click', function(){
        $('.popup').fadeIn();
      })
    }
		
		$doc.ready(init);
});